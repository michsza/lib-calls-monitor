#!/usr/bin/python3
import os, sys
import re
import datetime
import argparse

cwd = os.path.dirname(os.path.realpath(__file__)) + "/"

def valid_file_path(path):
    if not path.__contains__("/"):
        path = cwd + path 
    else:
        path = os.path.abspath(path)
    if not os.path.isfile(path):
        raise argparse.ArgumentTypeError("File:{0} is not a valid file/path".format(path))
    if os.access(path, os.R_OK):
        return path
    else:
        raise argparse.ArgumentTypeError("File:{0} is not readable".format(path))

def create_arg_parser():
    argparser = argparse.ArgumentParser(description='Parse stap library calls log')
    argparser.add_argument('--stap_log', '-sl', type=valid_file_path, required=True, help='Path to stap log')
    argparser.add_argument('--symbols_raw','-sr', type=valid_file_path, required=True, help='Path to raw symbols file')
    argparser.add_argument('--symbols_decoded','-sd', type=valid_file_path, required=True, help='Path to decoded symbols file')
    return argparser

def parser(log_path, symbols_path, decoded_symbols_path):
    time = datetime.datetime.now().time()
    time_stamp = time.strftime("%H%M%S") 
    call_parsed_log = log_path.split("/")[-1] + "_parsed" + time_stamp

    library_symbols_dict = {}
    if os.path.getsize(symbols_path) == 0:
        print("File of raw symbols: {0} is empty! Exiting.".format(symbols_path))
        sys.exit()
    if os.path.getsize(decoded_symbols_path) == 0:
        print("File of decoded symbols: {0} is empty! Exiting.".format(decoded_symbols_path))
        sys.exit()

    with open(symbols_path, 'r') as f_raw, open(decoded_symbols_path, 'r') as f_decoded:
        for line1, line2 in zip(f_raw.readlines(), f_decoded.readlines()):
            el_raw_list = line1.split()
            el_decode_list = line2.split()
            if len(el_raw_list) > 2:
                library_symbols_dict[' '.join(el_raw_list[2:])] = ' '.join(el_decode_list[2:])

    fnc_find_re = re.compile(r'"(.*?)"')

    with open(os.path.join(cwd+call_parsed_log), 'a') as parsedlog:
        with open(log_path, 'r') as f:
            for line in f.readlines():
                if line.__contains__('calls['):
                    function_found = fnc_find_re.search(line)
                    function = function_found.group(0).replace('"', '')

                    if function in library_symbols_dict:
                        line = line.replace(function,library_symbols_dict[function])
                    parsedlog.write(line + '\n')

    print("\n Parsing done. Parsed file available at: \n {0}".format(cwd+call_parsed_log))

if __name__ == "__main__":
    arg_parser = create_arg_parser()
    parsed_args = arg_parser.parse_args()
    parser( parsed_args.stap_log, 
            parsed_args.symbols_raw, 
            parsed_args.symbols_decoded
            )




    

