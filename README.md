# Monitor shared library specific calls using SystemTap

Instruction: 

1. Prepare raw symbols of library:

    nm -D <library_path> > raw_symbols_file 
<br>
2. Parse raw symbols to get demangled version:

    cat raw_symbols_file | c++filt > demangled_symbols_file 
<br>
3. Collect stap_log for specific workload which uses the lib:

    stap -c <workload/test which will be executed and use the lib> stap_call_count.stp <library_path> <string of symbol to monitor> > stap_calls_log \
    e.g. : "sudo -E stap -v -c "python3 test.py" stap_call_count.stp ~/lib.so "string" > stap_calls_log"

    Here are <string of symbol to monitor> examples: 
     - "*string*" - all calls for functions that symbol contains 'string'
     - "xyz" - all calls for function that symbols is 'xyz'
     - "*" - all calls to library (be careful cause it can provide inaccurate/incorrect results or overload of probes)

    Additional info: \
        stap_call_count.stp is script that provides stap functions for counting and measuring time of library function \
        The time function: gettimeofday_ms() provides results in ms. \
        It can be changed to ns,us,s using other function from times (chapter 3.):

        https://sourceware.org/systemtap/tapsets/
<br>
4. Parse stap_calls_log to decode raw symbols and replate it by demangle:

        python3 stap_lib_calls_parser.py -sl <stap_calls_log_path> -sr <raw_symbols_file__path> -sd <demangled_symbols_file_path>
<br>
References:

 - https://en.wikipedia.org/wiki/SystemTap

 - https://lwn.net/Articles/315022/

 - https://sourceware.org/systemtap/



